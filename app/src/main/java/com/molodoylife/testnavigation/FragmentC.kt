package com.molodoylife.testnavigation

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_c.*


class FragmentC : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d("Tag", "FragmentC onCreateView")
        return inflater.inflate(R.layout.fragment_c, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d("Tag", "FragmentC onViewCreated")

        buttonC.setOnClickListener{
            findNavController().navigate(R.id.action_fragmentC_to_fragmentD)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("Tag", "FragmentC onCreate")
        super.onCreate(savedInstanceState)
    }

    override fun onAttach(context: Context) {
        Log.d("Tag", "FragmentC onAttach()")
        super.onAttach(context)
    }

    override fun onDetach() {
        Log.d("Tag", "FragmentС onDetach")
        super.onDetach()
    }

    override fun onDestroy() {
        Log.d("Tag", "FragmentC onDestroy")
        super.onDestroy()
    }

    override fun onStart() {
        Log.d("Tag", "FragmentC onStart")
        super.onStart()
    }

    override fun onStop() {
        Log.d("Tag", "FragmentC onStop")
        super.onStop()
    }

    override fun onDestroyView() {
        Log.d("Tag", "FragmentC onDestroyView")
        super.onDestroyView()
    }
}
package com.molodoylife.testnavigation

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_d.*

class FragmentD : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        Log.d("Tag", "FragmentD onCreateView")
        return inflater.inflate(R.layout.fragment_d, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        Log.d("Tag", "FragmentD onViewCreated")

        buttonD.setOnClickListener{
            findNavController().navigateUp()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("Tag", "FragmentD onCreate")
        super.onCreate(savedInstanceState)
    }

    override fun onAttach(context: Context) {
        Log.d("Tag", "FragmentD onAttach()")
        super.onAttach(context)
    }

    override fun onDetach() {
        Log.d("Tag", "FragmentD onDetach")
        super.onDetach()
    }

    override fun onDestroy() {
        Log.d("Tag", "FragmentD onDestroy")
        super.onDestroy()
    }

    override fun onStart() {
        Log.d("Tag", "FragmentD onStart")
        super.onStart()
    }

    override fun onStop() {
        Log.d("Tag", "FragmentD onStop")
        super.onStop()
    }

    override fun onDestroyView() {
        Log.d("Tag", "FragmentD onDestroyView")
        super.onDestroyView()
    }
}
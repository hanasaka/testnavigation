package com.molodoylife.testnavigation

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment

class FragmentA : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_a, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("Tag", "FragmentA onCreate")
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d("Tag", "FragmentA onViewCreated")
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onAttach(context: Context) {
        Log.d("Tag", "FragmentA onAttach()")
        super.onAttach(context)
    }

    override fun onDetach() {
        Log.d("Tag", "FragmentA onDetach")
        super.onDetach()
    }

    override fun onDestroy() {
        Log.d("Tag", "FragmentA onDestroy")
        super.onDestroy()
    }

    override fun onStart() {
        Log.d("Tag", "FragmentA onStart")
        super.onStart()
    }

    override fun onStop() {
        Log.d("Tag", "FragmentA onStop")
        super.onStop()
    }

    override fun onDestroyView() {
        Log.d("Tag", "FragmentA onDestroyView")
        super.onDestroyView()
    }
}
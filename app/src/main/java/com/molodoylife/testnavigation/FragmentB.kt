package com.molodoylife.testnavigation

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_b.*
class FragmentB : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_b, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d("Tag", "FragmentB onViewCreated")
        super.onViewCreated(view, savedInstanceState)

        buttonB.setOnClickListener {
            findNavController().navigate(R.id.action_fragmentB_to_fragmentC)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("Tag", "FragmentB onCreate")
        super.onCreate(savedInstanceState)
    }

    override fun onAttach(context: Context) {
        Log.d("Tag", "FragmentB onAttach()")
        super.onAttach(context)
    }

    override fun onDetach() {
        Log.d("Tag", "FragmentB onDetach")
        super.onDetach()
    }

    override fun onDestroy() {
        Log.d("Tag", "FragmentB onDestroy")
        super.onDestroy()
    }

    override fun onStart() {
        Log.d("Tag", "FragmentB onStart")
        super.onStart()
    }

    override fun onStop() {
        Log.d("Tag", "FragmentB onStop")
        super.onStop()
    }

    override fun onDestroyView() {
        Log.d("Tag", "FragmentB onDestroyView")
        super.onDestroyView()
    }
}